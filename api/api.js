let util = require('../util.json');
let ObjectID = require('mongodb').ObjectID;
let xlsx = require('xlsx');
let dbCollections = util.dbCollectionName;

let self = module.exports = {

    readExcelFile : function(req,res) {
    let workbook = xlsx.readFile('./public/excelFiles/finalFormat.xlsx');
    let worksheet = workbook.Sheets['Sheet1'];
    let jsonSheet = xlsx.utils.sheet_to_json(worksheet);
    jsonSheet.splice(0,1);
    
    self.formatJsonKeys(jsonSheet,function(err,result){
        if(err) {
            res.send(err);
            return;
        }
        self.insertExcelDataInDatabase(result,function(err,insertionResult){
            if(err) {
                res.send(err);
                return;
            }
            res.send(insertionResult);
        })
    })
    
    },

    formatJsonKeys : function(dataArray,callback) {
        if(dataArray.length === 0) {
            callback(util.noDataFound);
            return;
        }

        let formattedJson = [];
        let JsonMapping = util.jsonKeysMapping;

        for(let i = 0; i < dataArray.length; i++) {
            let formattedObject = {
                "moduleName": dataArray[i][JsonMapping.moduleName] || "",
                "noOfInitiatives": dataArray[i][JsonMapping.noOfInitiatives] || "",
                "initiativeName": dataArray[i][JsonMapping.initiativeName] || "",
                "estimateLowerLimit": dataArray[i][JsonMapping.estimateLowerLimit] || "",
                "estimateUpperLimit": dataArray[i][JsonMapping.estimateUpperLimit] || "",
                "valueStatus": dataArray[i][JsonMapping.valueStatus] || "",
                "nextSteps": dataArray[i][JsonMapping.nextSteps] || "",
                "bsidFpr": dataArray[i][JsonMapping.bsidFpr] || "",
                "timeLine": dataArray[i][JsonMapping.timeLine] || "",
                "progressStatus": dataArray[i][JsonMapping.progressStatus] || "",
                "noOfDaysDelayed": dataArray[i][JsonMapping.noOfDaysDelayed] || 0,
                "noOfDaysExpedited": dataArray[i][JsonMapping.noOfDaysExpedited] || 0,
                "costOfDelay": Math.round(dataArray[i][JsonMapping.costOfDelay]) || 0,
                "gainFromExpedition": Math.round(dataArray[i][JsonMapping.gainFromExpedition]) || 0
            }

            formattedJson.push(formattedObject);
            formattedObject = {};
        }

        callback(null,formattedJson);
    },

    insertExcelDataInDatabase : function(formattedData, callback) {
        if(formattedData.length === 0) {
            callback(util.noDataFound);
            return;
        }
        const excelDataCollection = db.collection(dbCollections.excelData);

        excelDataCollection.insertMany(formattedData, function (err, inserted) {
            if (err) {
                res.send(util.databaseError);
                return;
            }
            callback(null,util.insertionSuccessfull);
        });
    },

    getUploadedData : function(req,res) {
        const excelDataCollection = db.collection(dbCollections.excelData);

        excelDataCollection.find().toArray(function (err, data) {
            if (err) {
                res.send(util.databaseError);
                return;
            }
            var result = self.groupBy(data, function (item) {
                return [item.moduleName, item.noOfInitiatives, item.valueStatus, item.estimateLowerLimit, item.estimateUpperLimit];
            });

            let groupedObjects = []

            for(let i = 0; i < result.length; i++) {
                // let obj = {
                //     "noOfInitiatives" : result[i][0].noOfInitiatives, 
                //     "valueStatus" : result[i][0].valueStatus, 
                //     "estimateLowerLimit" : result[i][0].estimateLowerLimit, 
                //     "estimateUpperLimit" : result[i][0].estimateUpperLimit,
                //     "nextSteps": result[i][0].nextSteps,
                //     "bsidFpr": result[i][0].bsidFpr,
                //     "timeLine": result[i][0].timeLine,
                //     "progressStatus": result[i][0].progressStatus,
                //     "noOfDaysDelayed": result[i][0].noOfDaysDelayed,
                //     "noOfDaysExpedited": result[i][0].noOfDaysExpedited,
                //     "costOfDelay": result[i][0].costOfDelay,
                //     "gainFromExpedition": result[i][0].gainFromExpedition,
                //     "data" : result[i]
                // }

                let obj = {...result[i][0],data:result[i]}

                let newProgressStatus = self.calculateProgressStatus(result[i]);

                obj.progressStatus = newProgressStatus;

                groupedObjects.push(obj);
                obj = {};
            }

            res.send({...util.fetchDataResponse,'results': groupedObjects});
        });
    },

    calculateProgressStatus : function(dataObject) {
        if(dataObject.length === 0) {
            return util.progressStatus.onTrack;
        }

        let delayedCount = 0;
        let yetToStartCount = 0;
        let completedCount = 0;

        for(let i = 0; i < dataObject.length; i++) {
            let obj = dataObject[i];
            if(obj.progressStatus.toLowerCase() === util.progressStatus.delayed.toLowerCase()){
                delayedCount = delayedCount + 1;
            }

            if(obj.progressStatus.toLowerCase() === util.progressStatus.yetToStart.toLowerCase()){
                yetToStartCount = yetToStartCount + 1;
            }

            if(obj.progressStatus.toLowerCase() === util.progressStatus.completed.toLowerCase()){
                completedCount = completedCount + 1;
            }
        }
        
        if(delayedCount > 0) {
            return util.progressStatus.delayed;
        }
        else if(yetToStartCount === dataObject.length) {
            return util.progressStatus.yetToStart;
        }
        else if(completedCount === dataObject.length) {
            return util.progressStatus.completed;
        }
        else {
            return util.progressStatus.onTrack;
        }

    },

    groupBy: function (array, f) {
        var groups = {};
        array.forEach(function (o) {
            var group = JSON.stringify(f(o));
            groups[group] = groups[group] || [];
            groups[group].push(o);
        });
        return Object.keys(groups).map(function (group) {
            return groups[group];
        })
    },

    getFilteredData : function(req,res) {

        let requestBody = req.body;
        var query = self.createQuery(requestBody);
        const excelDataCollection = db.collection(dbCollections.excelData);

        excelDataCollection.find(query).toArray(function (err, data) {
            if (err) {
                res.send(util.databaseError);
                return;
            }
            var result = self.groupBy(data, function (item) {
                return [item.noOfInitiatives, item.valueStatus, item.estimateLowerLimit, item.estimateUpperLimit];
            });

            let groupedObjects = []

            for(let i = 0; i < result.length; i++) {

                let obj = {...result[i][0],data:result[i]}
                let newProgressStatus = self.calculateProgressStatus(result[i]);

                obj.progressStatus = newProgressStatus;

                if(requestBody.progressStatus === undefined || requestBody.progressStatus === '') {
                    groupedObjects.push(obj);
                }
                else {
                    if(requestBody.progressStatus.indexOf(obj.progressStatus) > -1) {
                        groupedObjects.push(obj);
                    }
                }
               
                obj = {};
            }

            res.send({...util.fetchDataResponse,'results': groupedObjects});
        });

    },

    createQuery : function (param) {
        var fieldArray = ['moduleName', 'valueStatus', 'initiativeName', 'bsidFpr'];
        var objectKeys = Object.keys(param);
        var query = {};
    
        for (let i = 0; i < fieldArray.length; i++) {
    
            if (objectKeys.indexOf(fieldArray[i]) === -1 || param[fieldArray[i]] === "") {
                continue;
            }
            //var pattern = new RegExp(param[fieldArray[i]], "i");
            var queryString = fieldArray[i]
            query[queryString] = {$in: param[fieldArray[i]] };
        }
        return query
    },

    overallValueStatus : function(req,res) {
        let requestBody = req.body;
        self.calculateAverageLimitForValueStatus(function(err,averageLimitResult){
            if (err) {
                res.send(err);
                return;
            }
            self.calculateNoOfInitiativesForValueStatus(function(err,noOfInitiatives) {
                if (err) {
                    res.send(err);
                    return;
                }

                for(let i = 0; i < averageLimitResult.length; i++) {
                    averageLimitResult[i].noOfInitiatives = noOfInitiatives[averageLimitResult[i].valueStatus];
                }

                res.send({...util.fetchDataResponse,'overallStatus': averageLimitResult});
            });
        })
        
    },

    
    calculateAverageLimitForValueStatus : function(callback) {
        const excelDataCollection = db.collection(dbCollections.excelData);

        excelDataCollection.find().toArray(function (err, data) {
            if (err) {
                res.send(util.databaseError);
                return;
            }

            var result = self.groupBy(data, function (item) {
                return [item.valueStatus];
            });

            let resultArray = [];
            for(let i = 0; i < result.length; i++) {
                let arrayData = result[i];
                let totalLowerLimit = 0;
                let totalUpperLimit = 0;
                let averageLimit = 0;
                let countedValue = [];
                for(let j = 0; j < arrayData.length; j++) {
                    if(countedValue.indexOf(arrayData[j].noOfInitiatives) === -1) {
                    totalLowerLimit = totalLowerLimit + arrayData[j].estimateLowerLimit;
                    totalUpperLimit = totalUpperLimit + arrayData[j].estimateUpperLimit;
                    countedValue.push(arrayData[j].noOfInitiatives);
                    }
                }

                averageLimit = (totalLowerLimit + totalUpperLimit) / 2;

                let obj = {
                    "valueStatus" : arrayData[0].valueStatus,
                    "loweLimit" : Math.round(totalLowerLimit),
                    "upperLimit" : Math.round(totalUpperLimit),
                    "averageLimit" : Math.round(averageLimit)
                }
                resultArray.push(obj);
                obj = {};
            }
            callback(null,resultArray);
        });
    },

    calculateNoOfInitiativesForValueStatus : function(callback) {
        const excelDataCollection = db.collection(dbCollections.excelData);

        excelDataCollection.find().toArray(function (err, data) {
            if (err) {
                res.send(util.databaseError);
                return;
            }

            var result = self.groupBy(data, function (item) {
                return [item.valueStatus];
            });

            let obj = {}
            for(let i = 0; i < result.length; i++) {
                let arrayData = result[i];
                let count = 0;
                let countedValue = [];
                for(let j = 0; j < arrayData.length; j++) {
                   if(countedValue.indexOf(arrayData[j].noOfInitiatives) === -1) {
                       count = count + 1;
                       countedValue.push(arrayData[j].noOfInitiatives);
                   }
                }
                obj[arrayData[0].valueStatus] = count;
            }
            callback(null,obj);
        });
    },

    moduleWiseValueGraph : function(callback) {
        const excelDataCollection = db.collection(dbCollections.excelData);

        excelDataCollection.find().toArray(function (err, data) {
            if (err) {
                res.send(util.databaseError);
                return;
            }

            var result = self.groupBy(data, function (item) {
                return [item.valueStatus];
            });

            let resultArray = [];
            for(let i = 0; i < result.length; i++) {
                let arrayData = result[i];
                let totalLowerLimit = 0;
                let totalUpperLimit = 0;
                let averageLimit = 0;
                let countedValue = [];
                for(let j = 0; j < arrayData.length; j++) {
                    if(countedValue.indexOf(arrayData[j].noOfInitiatives) === -1) {
                    totalLowerLimit = totalLowerLimit + arrayData[j].estimateLowerLimit;
                    totalUpperLimit = totalUpperLimit + arrayData[j].estimateUpperLimit;
                    countedValue.push(arrayData[j].noOfInitiatives);
                    }
                }

                averageLimit = (totalLowerLimit + totalUpperLimit) / 2;

                let obj = {
                    "valueStatus" : arrayData[0].valueStatus,
                    "averageLimit" : Math.round(averageLimit)
                }
                resultArray.push(obj);
                obj = {};
            }
            callback(null,resultArray);
        });
    },
    
    getFilterDropDownValues : function(req,res) {
        const excelDataCollection = db.collection(dbCollections.excelData);
        let finalResult = {"code" : 200};
        excelDataCollection.distinct("moduleName",function(err,moduleNames) {
            if (err) {
                res.send(util.databaseError);
                return;
            }
            
            finalResult.moduleName = moduleNames;

            excelDataCollection.distinct("initiativeName",function(err,initiativeNames) {
                if (err) {
                    res.send(util.databaseError);
                    return;
                }
                
                finalResult.initiativeName = initiativeNames;

                excelDataCollection.distinct("valueStatus",function(err,valueStatus) {
                    if (err) {
                        res.send(util.databaseError);
                        return;
                    }
                    
                    finalResult.valueStatus = valueStatus;

                    excelDataCollection.distinct("bsidFpr",function(err,bsidFpr) {
                        if (err) {
                            res.send(util.databaseError);
                            return;
                        }
                        
                        finalResult.bsidFpr = bsidFpr;

                        excelDataCollection.distinct("progressStatus",function(err,progressStatus) {
                            if (err) {
                                res.send(util.databaseError);
                                return;
                            }
                            
                            finalResult.progressStatus = progressStatus;
                            res.send(finalResult);
                        })
                    })
                })
            })
        })
    },

    progressStatusPercentage : function(req,res) {
       
        const excelDataCollection = db.collection(dbCollections.excelData);
        let completedLowerLimitTotal = 0;
        let completedUpperLimitTotal = 0;
        let ontrackLowerLimitTotal = 0;
        let ontrackUpperLimitTotal = 0;
        let delayedLowerLimitTotal = 0;
        let delayedUpperLimitTotal = 0;
        let yetToStartLowerLimitTotal = 0;
        let yetToStartUpperLimitTotal = 0;
        let totalLowerLimit = 0;
        let totalUpperLimit = 0

        excelDataCollection.find().toArray(function (err, data) {
            if (err) {
                res.send(util.databaseError);
                return;
            }
            var result = self.groupBy(data, function (item) {
                return [item.moduleName, item.noOfInitiatives, item.valueStatus, item.estimateLowerLimit, item.estimateUpperLimit];
            });

            let groupedObjects = []

            for(let i = 0; i < result.length; i++) {
                // let obj = {
                //     "noOfInitiatives" : result[i][0].noOfInitiatives, 
                //     "valueStatus" : result[i][0].valueStatus, 
                //     "estimateLowerLimit" : result[i][0].estimateLowerLimit, 
                //     "estimateUpperLimit" : result[i][0].estimateUpperLimit,
                //     "nextSteps": result[i][0].nextSteps,
                //     "bsidFpr": result[i][0].bsidFpr,
                //     "timeLine": result[i][0].timeLine,
                //     "progressStatus": result[i][0].progressStatus,
                //     "noOfDaysDelayed": result[i][0].noOfDaysDelayed,
                //     "noOfDaysExpedited": result[i][0].noOfDaysExpedited,
                //     "costOfDelay": result[i][0].costOfDelay,
                //     "gainFromExpedition": result[i][0].gainFromExpedition,
                //     "data" : result[i]
                // }

                let obj = {...result[i][0],data:result[i]}

                let newProgressStatus = self.calculateProgressStatus(result[i]);

                obj.progressStatus = newProgressStatus;

                if(newProgressStatus === util.progressStatus.delayed) {
                    delayedLowerLimitTotal = delayedLowerLimitTotal + obj.estimateLowerLimit;
                    delayedUpperLimitTotal = delayedUpperLimitTotal + obj.estimateUpperLimit;
                }
                else if(newProgressStatus === util.progressStatus.yetToStart) {
                    yetToStartLowerLimitTotal = yetToStartLowerLimitTotal + obj.estimateLowerLimit;
                    yetToStartUpperLimitTotal = yetToStartUpperLimitTotal + obj.estimateUpperLimit;
                }
                else if(newProgressStatus === util.progressStatus.completed) {
                    completedLowerLimitTotal = completedLowerLimitTotal + obj.estimateLowerLimit;
                    completedUpperLimitTotal = completedUpperLimitTotal + obj.estimateUpperLimit;
                }
                else if(newProgressStatus === util.progressStatus.onTrack) {
                    ontrackLowerLimitTotal = ontrackLowerLimitTotal + obj.estimateLowerLimit;
                    ontrackUpperLimitTotal = ontrackUpperLimitTotal + obj.estimateUpperLimit;
                }
                totalLowerLimit = totalLowerLimit + obj.estimateLowerLimit;
                totalUpperLimit = totalUpperLimit + obj.estimateUpperLimit;
                groupedObjects.push(obj);
                obj = {};
            }

            let averageCompleted = (completedLowerLimitTotal + completedUpperLimitTotal) / 2;
            let averageYetToStart = (yetToStartLowerLimitTotal + yetToStartUpperLimitTotal) / 2;
            let averageDelayed = (delayedLowerLimitTotal + delayedUpperLimitTotal) / 2;
            let averageOnTrack = (ontrackLowerLimitTotal + ontrackUpperLimitTotal) / 2;
            let averageTotalLimit = (totalLowerLimit + totalUpperLimit) / 2;
            let completedPercentage = (averageCompleted / averageTotalLimit) * 100;
            let yetToStartPercentage = (averageYetToStart / averageTotalLimit) * 100;
            let onTrackPercentage = (averageOnTrack / averageTotalLimit) * 100;
            let delayedPercentage = (averageDelayed / averageTotalLimit) * 100;

            res.send({'code':200,
            'completedPercentage': completedPercentage.toFixed(2),
            'yetToStartPercentage': yetToStartPercentage.toFixed(2),
            'onTrackPercentage': onTrackPercentage.toFixed(2),
            'delayedPercentage': delayedPercentage.toFixed(2),
            'completedLowerLimit' : Math.round(completedLowerLimitTotal),
            'completedUpperLimit' : Math.round(completedUpperLimitTotal),
            'yetToStartLowerLimit' : Math.round(yetToStartLowerLimitTotal),
            'yetToStartUpperLimit' : Math.round(yetToStartUpperLimitTotal),
            'delayedLowerLimit' : Math.round(delayedLowerLimitTotal),
            'delayedUpperLimit' : Math.round(delayedUpperLimitTotal),
            'onTrackLowerLimit' : Math.round(ontrackLowerLimitTotal),
            'onTrackUpperLimit' : Math.round(ontrackUpperLimitTotal)
        });
        });
    },

    moduleWiseValueStatusGraph : function(req,res) { 
        const excelDataCollection = db.collection(dbCollections.excelData);

        excelDataCollection.find().toArray(function (err, data) {
            if (err) {
                res.send(util.databaseError);
                return;
            }
            var result = self.groupBy(data, function (item) {
                return [item.moduleName, item.valueStatus];
            });

            let groupedObjects = []

            for(let i = 0; i < result.length; i++) {

                let arrayData = result[i];
                let totalLowerLimit = 0;
                let totalUpperLimit = 0;
                let averageLimit = 0;
                let countedValue = [];
                for(let j = 0; j < arrayData.length; j++) {
                    if(countedValue.indexOf(arrayData[j].noOfInitiatives) === -1) {
                    totalLowerLimit = totalLowerLimit + arrayData[j].estimateLowerLimit;
                    totalUpperLimit = totalUpperLimit + arrayData[j].estimateUpperLimit;
                    countedValue.push(arrayData[j].noOfInitiatives);
                    }
                 }
                 let obj = {
                    "moduleName" : arrayData[0].moduleName,
                    "averageLimit" : (totalLowerLimit + totalUpperLimit) / 2,
                    "valueStatus" : arrayData[0].valueStatus 
                }

                groupedObjects.push(obj);
                obj = {};
            }
            let unique = [...new Set(groupedObjects.map(item => item.moduleName))];

            let moduleStatusMap = {};
            let D0 = [];
            let D1 = [];
            let D2 = [];
            let D3 = [];
            let D4 = [];
            for(let j = 0; j < unique.length; j++) {
                let isValueAddedForD0 = false;
                let isValueAddedForD1 = false;
                let isValueAddedForD2 = false;
                let isValueAddedForD3 = false;
                let isValueAddedForD4 = false;
            for(let i = 0; i < groupedObjects.length; i++) {

                if(groupedObjects[i].moduleName === unique[j]){
                    if(groupedObjects[i].valueStatus === util.valueStatus.d0) {
                        D0.push(groupedObjects[i].averageLimit);
                        isValueAddedForD0 = true;
                    }
                    if(groupedObjects[i].valueStatus === util.valueStatus.d1) {
                        D1.push(groupedObjects[i].averageLimit);
                        isValueAddedForD1 = true;
                    }
                    if(groupedObjects[i].valueStatus === util.valueStatus.d2) {
                        D2.push(groupedObjects[i].averageLimit);
                        isValueAddedForD2 = true;
                    }
                    if(groupedObjects[i].valueStatus === util.valueStatus.d3) {
                        D3.push(groupedObjects[i].averageLimit);
                        isValueAddedForD3 = true;
                    }
                    if(groupedObjects[i].valueStatus === util.valueStatus.d4) {
                        D4.push(groupedObjects[i].averageLimit);
                        isValueAddedForD4 = true;
                    }
                }
            }
                if(!isValueAddedForD0){
                    D0.push(0);
                }
                if(!isValueAddedForD1){
                    D1.push(0);
                }
                if(!isValueAddedForD2){
                    D2.push(0);
                }
                if(!isValueAddedForD3){
                    D3.push(0);
                }
                if(!isValueAddedForD4){
                    D4.push(0);
                }
               // moduleStatusMap[groupedObjects[i].moduleName+ "_" + groupedObjects[i].valueStatus] = groupedObjects[i];
            }

            let finalArray = [];
            finalArray.push({"label": "D0", "data" : D0});
            finalArray.push({"label": "D1", "data" : D1});
            finalArray.push({"label": "D2", "data" : D2});
            finalArray.push({"label": "D3", "data" : D3});
            finalArray.push({"label": "D4", "data" : D4});

            res.send({...util.fetchDataResponse,'results': finalArray,'moduleName' : unique});
        });
    },

    moduleWiseNoOfInitiativesGraph : function(req,res) {
        const excelDataCollection = db.collection(dbCollections.excelData);

        excelDataCollection.find().toArray(function (err, data) {
            if (err) {
                res.send(util.databaseError);
                return;
            }
            var result = self.groupBy(data, function (item) {
                return [item.moduleName, item.valueStatus];
            });

            let groupedObjects = []

            for(let i = 0; i < result.length; i++) {

                let arrayData = result[i];
                let totalLowerLimit = 0;
                let totalUpperLimit = 0;
                let uniqueCount = 0;
                let countedValue = [];
                for(let j = 0; j < arrayData.length; j++) {
                    if(countedValue.indexOf(arrayData[j].noOfInitiatives) === -1) {
                        uniqueCount = uniqueCount + 1;
                    countedValue.push(arrayData[j].noOfInitiatives);
                    }
                 }
                 let obj = {
                    "moduleName" : arrayData[0].moduleName,
                    "uniqueCount" : uniqueCount,
                    "valueStatus" : arrayData[0].valueStatus 
                }

                groupedObjects.push(obj);
                obj = {};
            }
            let unique = [...new Set(groupedObjects.map(item => item.moduleName))];

            let moduleStatusMap = {};
            let D0 = [];
            let D1 = [];
            let D2 = [];
            let D3 = [];
            let D4 = [];
            for(let j = 0; j < unique.length; j++) {
                let isValueAddedForD0 = false;
                let isValueAddedForD1 = false;
                let isValueAddedForD2 = false;
                let isValueAddedForD3 = false;
                let isValueAddedForD4 = false;
            for(let i = 0; i < groupedObjects.length; i++) {

                if(groupedObjects[i].moduleName === unique[j]){
                    if(groupedObjects[i].valueStatus === util.valueStatus.d0) {
                        D0.push(groupedObjects[i].uniqueCount);
                        isValueAddedForD0 = true;
                    }
                    if(groupedObjects[i].valueStatus === util.valueStatus.d1) {
                        D1.push(groupedObjects[i].uniqueCount);
                        isValueAddedForD1 = true;
                    }
                    if(groupedObjects[i].valueStatus === util.valueStatus.d2) {
                        D2.push(groupedObjects[i].uniqueCount);
                        isValueAddedForD2 = true;
                    }
                    if(groupedObjects[i].valueStatus === util.valueStatus.d3) {
                        D3.push(groupedObjects[i].uniqueCount);
                        isValueAddedForD3 = true;
                    }
                    if(groupedObjects[i].valueStatus === util.valueStatus.d4) {
                        D4.push(groupedObjects[i].uniqueCount);
                        isValueAddedForD4 = true;
                    }
                }
            }
                if(!isValueAddedForD0){
                    D0.push(0);
                }
                if(!isValueAddedForD1){
                    D1.push(0);
                }
                if(!isValueAddedForD2){
                    D2.push(0);
                }
                if(!isValueAddedForD3){
                    D3.push(0);
                }
                if(!isValueAddedForD4){
                    D4.push(0);
                }
               // moduleStatusMap[groupedObjects[i].moduleName+ "_" + groupedObjects[i].valueStatus] = groupedObjects[i];
            }

            let finalArray = [];
            //finalArray.push({"label": "D0", "data" : D0});
            //finalArray.push({"label": "D1", "data" : D1});
            finalArray.push({"label": "D2", "data" : D2});
            finalArray.push({"label": "D3", "data" : D3});
            finalArray.push({"label": "D4", "data" : D4});

            res.send({...util.fetchDataResponse,'results': finalArray,'moduleName' : unique});
        });
    },

    moduleWiseProgressStatusAverageLimitGraph : function(req,res) {
        const excelDataCollection = db.collection(dbCollections.excelData);

        excelDataCollection.find().toArray(function (err, data) {
            if (err) {
                res.send(util.databaseError);
                return;
            }


            let groupedBeforeStatus = []

            var resultBeforStatus = self.groupBy(data, function (item) {
                return [item.moduleName, item.noOfInitiatives, item.valueStatus, item.estimateLowerLimit, item.estimateUpperLimit];
            });

            for(let i = 0; i < resultBeforStatus.length; i++) {

                let obj = {...resultBeforStatus[i][0],data:resultBeforStatus[i]}

                let newProgressStatus = self.calculateProgressStatus(resultBeforStatus[i]);

                obj.progressStatus = newProgressStatus;

                groupedBeforeStatus.push(obj);
                obj = {};
            }
            var result = self.groupBy(groupedBeforeStatus, function (item) {
                return [item.moduleName, item.progressStatus];
            });

            let groupedObjects = []

            for(let i = 0; i < result.length; i++) {

                let arrayData = result[i];
                let totalLowerLimit = 0;
                let totalUpperLimit = 0;
                let averageLimit = 0;
                let countedValue = [];
                for(let j = 0; j < arrayData.length; j++) {
                    if(countedValue.indexOf(arrayData[j].noOfInitiatives) === -1) {
                    totalLowerLimit = totalLowerLimit + arrayData[j].estimateLowerLimit;
                    totalUpperLimit = totalUpperLimit + arrayData[j].estimateUpperLimit;
                    countedValue.push(arrayData[j].noOfInitiatives);
                    }
                 }
                 let obj = {
                    "moduleName" : arrayData[0].moduleName,
                    "averageLimit" : (totalLowerLimit + totalUpperLimit) / 2,
                    "progressStatus" : arrayData[0].progressStatus 
                }

                groupedObjects.push(obj);
                obj = {};
            }
            let unique = [...new Set(groupedObjects.map(item => item.moduleName))];

            let onTrack = [];
            let delayed = [];
            let yetToStart = [];
            let completed = [];
            let D4 = [];
            for(let j = 0; j < unique.length; j++) {
                let isValueAddedForOnTrack = false;
                let isValueAddedForDelayed = false;
                let isValueAddedForYetToStart = false;
                let isValueAddedForCompleted = false;
                let isValueAddedForD4 = false;
            for(let i = 0; i < groupedObjects.length; i++) {

                if(groupedObjects[i].moduleName === unique[j]){
                    if(groupedObjects[i].progressStatus === util.progressStatus.onTrack) {
                        onTrack.push(groupedObjects[i].averageLimit);
                        isValueAddedForOnTrack = true;
                    }
                    if(groupedObjects[i].progressStatus === util.progressStatus.delayed) {
                        delayed.push(groupedObjects[i].averageLimit);
                        isValueAddedForDelayed = true;
                    }
                    if(groupedObjects[i].progressStatus === util.progressStatus.yetToStart) {
                        yetToStart.push(groupedObjects[i].averageLimit);
                        isValueAddedForYetToStart = true;
                    }
                    if(groupedObjects[i].progressStatus === util.progressStatus.completed) {
                        completed.push(groupedObjects[i].averageLimit);
                        isValueAddedForCompleted = true;
                    }
                }
            }
                if(!isValueAddedForOnTrack){
                    onTrack.push(0);
                }
                if(!isValueAddedForDelayed){
                    delayed.push(0);
                }
                if(!isValueAddedForYetToStart){
                    yetToStart.push(0);
                }
                if(!isValueAddedForCompleted){
                    completed.push(0);
                }
               // moduleStatusMap[groupedObjects[i].moduleName+ "_" + groupedObjects[i].valueStatus] = groupedObjects[i];
            }

            let finalArray = [];
            finalArray.push({"label": "On Track", "data" : onTrack});
            finalArray.push({"label": "Delayed", "data" : delayed});
            finalArray.push({"label": "Yet to start", "data" : yetToStart});
            finalArray.push({"label": "Completed", "data" : completed});

            res.send({...util.fetchDataResponse,'results': finalArray,'moduleName' : unique});
        });
    },

    moduleWiseProgressStatusCount : function(req,res) {
        const excelDataCollection = db.collection(dbCollections.excelData);

        excelDataCollection.find().toArray(function (err, data) {
            if (err) {
                res.send(util.databaseError);
                return;
            }


            let groupedBeforeStatus = []

            var resultBeforStatus = self.groupBy(data, function (item) {
                return [item.moduleName, item.noOfInitiatives, item.valueStatus, item.estimateLowerLimit, item.estimateUpperLimit];
            });

            for(let i = 0; i < resultBeforStatus.length; i++) {

                let obj = {...resultBeforStatus[i][0],data:resultBeforStatus[i]}

                let newProgressStatus = self.calculateProgressStatus(resultBeforStatus[i]);

                obj.progressStatus = newProgressStatus;

                groupedBeforeStatus.push(obj);
                obj = {};
            }
            var result = self.groupBy(groupedBeforeStatus, function (item) {
                return [item.moduleName, item.progressStatus];
            });

            let groupedObjects = []

            for(let i = 0; i < result.length; i++) {

                let arrayData = result[i];
                let totalLowerLimit = 0;
                let totalUpperLimit = 0;
                let averageLimit = 0;
                let uniqueCount = 0;
                let countedValue = [];
                for(let j = 0; j < arrayData.length; j++) {
                    if(countedValue.indexOf(arrayData[j].noOfInitiatives) === -1) {
                    uniqueCount = uniqueCount + 1;
                    countedValue.push(arrayData[j].noOfInitiatives);
                    }
                 }
                 let obj = {
                    "moduleName" : arrayData[0].moduleName,
                    "uniqueCount" : uniqueCount,
                    "progressStatus" : arrayData[0].progressStatus 
                }

                groupedObjects.push(obj);
                obj = {};
            }
            let unique = [...new Set(groupedObjects.map(item => item.moduleName))];

            let onTrack = [];
            let delayed = [];
            let yetToStart = [];
            let completed = [];
            let D4 = [];
            for(let j = 0; j < unique.length; j++) {
                let isValueAddedForOnTrack = false;
                let isValueAddedForDelayed = false;
                let isValueAddedForYetToStart = false;
                let isValueAddedForCompleted = false;
                let isValueAddedForD4 = false;
            for(let i = 0; i < groupedObjects.length; i++) {

                if(groupedObjects[i].moduleName === unique[j]){
                    if(groupedObjects[i].progressStatus === util.progressStatus.onTrack) {
                        onTrack.push(groupedObjects[i].uniqueCount);
                        isValueAddedForOnTrack = true;
                    }
                    if(groupedObjects[i].progressStatus === util.progressStatus.delayed) {
                        delayed.push(groupedObjects[i].uniqueCount);
                        isValueAddedForDelayed = true;
                    }
                    if(groupedObjects[i].progressStatus === util.progressStatus.yetToStart) {
                        yetToStart.push(groupedObjects[i].uniqueCount);
                        isValueAddedForYetToStart = true;
                    }
                    if(groupedObjects[i].progressStatus === util.progressStatus.completed) {
                        completed.push(groupedObjects[i].uniqueCount);
                        isValueAddedForCompleted = true;
                    }
                }
            }
                if(!isValueAddedForOnTrack){
                    onTrack.push(0);
                }
                if(!isValueAddedForDelayed){
                    delayed.push(0);
                }
                if(!isValueAddedForYetToStart){
                    yetToStart.push(0);
                }
                if(!isValueAddedForCompleted){
                    completed.push(0);
                }
               // moduleStatusMap[groupedObjects[i].moduleName+ "_" + groupedObjects[i].valueStatus] = groupedObjects[i];
            }

            let finalArray = [];
            finalArray.push({"label": "On Track", "data" : onTrack});
            finalArray.push({"label": "Delayed", "data" : delayed});
            finalArray.push({"label": "Yet to start", "data" : yetToStart});
            finalArray.push({"label": "Completed", "data" : completed});

            res.send({...util.fetchDataResponse,'results': finalArray,'moduleName' : unique});
        });
    }
};