process.on('uncaughtException', function (err) {
  console.log(err);
});
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongodb = require('mongodb').MongoClient;
var multer  = require('multer')
var api = require('./api/api');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./public/swagger.json');


const config = require('./config.json');

var index = require('./routes/index');
var users = require('./routes/users');

var app = express();

// Connection URL
const url = config.mongodb.url;
 
// Database Name
const dbName = config.mongodb.dbName;
 
//Use connect method to connect to the server
mongodb.connect(url, function(err, client) {
  console.log("Connected successfully to Database server");
  const dbConnection = client.db(dbName);
  global.db = dbConnection;
});

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    /* res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');*/
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "content-type, accept");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "Content-Type, X-Requested-With, X-PINGOTHER, X-File-Name, Cache-Control");
    if ('OPTIONS' === req.method) {
        res.writeHead('200');
        res.end();
    } else {
        next();
    }
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);
app.use('/readFile',api.readExcelFile);
app.use('/getUploadedData',api.getUploadedData);
app.use('/getFilteredData',api.getFilteredData);
app.use('/overallStatusDashboard',api.overallValueStatus);
app.use('/getFilterDropDown',api.getFilterDropDownValues);
app.use('/progressStatusPercentage',api.progressStatusPercentage);
app.use('/moduleWiseValueStatusGraph',api.moduleWiseValueStatusGraph);
app.use('/moduleWiseNoOfInitiativesGraph',api.moduleWiseNoOfInitiativesGraph);
app.use('/moduleWiseProgressStatus',api.moduleWiseProgressStatusAverageLimitGraph);
app.use('/moduleWiseProgressStatusCount',api.moduleWiseProgressStatusCount);
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
